import sys
import streamlit as st
from core.MultiApp import App

smalls={
    'Nothing': "Nothing...",
}

myapp = App("report", "report", smalls)

st.set_page_config(
page_title="Report",
page_icon=":clipboard:",
layout="wide"
)

myapp.main()
