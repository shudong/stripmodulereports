### standard
import streamlit as st
from core.Page import Page
### custom
import altair as alt
import copy
import pandas as pd
import numpy as np
import datetime
from itkdb.exceptions import BadRequest
import time
import utils.Report.report_tools as report_tools
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
#####################
### useful functions
#####################

infoList=["  * Production database report...",
        "   * Nothing here..."]

testTypes={"HYBRID_ASSEMBLY":("ASIC_GLUE_WEIGHT","ASIC_GLUE_HEIGHT", "INPUT_NOISE", "WIRE_BONDING"),
                "MODULE":("GLUE_WEIGHT", "GLUE_HEIGHT", "WIRE_BONDING", "IV_TEST_CONSISTENCY"),
        }

def FormatTestRunData_GlueWeight(dfTestRuns, compType, compSubType):
    paraCode=''
    moduleParaCodeDict={
            'GW_GLUE_H1H2PB': ['BARREL_SS_MODULE', 'R0', 'R1', 'R3M0_HALFMODULE'],
            'GW_GLUE_H1PB': ['BARREL_LS_MODULE', 'R2', 'R4M0_HALFMODULE', 'R5M0_HALFMODULE'],
            'GW_GLUE_H1': ['R4M1_HALFMODULE', 'R5M1_HALFMODULE'], ### expect to always stored in H1
            'GW_GLUE_H1H2': ['R3M1_HALFMODULE']
    }

    if compType=='HYBRID_ASSEMBLY':
        paraCode='GW_GLUE_ASICS'
    elif compType=='MODULE':
        for k,v in moduleParaCodeDict.items():
            if compSubType in v:
                paraCode=k
                break
    else:
        st.warning('Unsupported component type for Glue Weight test!')
        return pd.DataFrame()
    if paraCode=='':
        return pd.DataFrame()

    dfTestRuns=dfTestRuns[dfTestRuns['compSubTypeCode']==compSubType]
    dfTestRuns['GW_GLUE_ALL']=dfTestRuns.apply(lambda row: row['paraValue'] if row['paraCode']==paraCode else np.nan, axis=1)
    dfTestRuns=dfTestRuns[~dfTestRuns['GW_GLUE_ALL'].isna()]
    dfTestRuns=dfTestRuns.reset_index(drop=True)
    
    return dfTestRuns

def PlotGlueWeight(dfs_gw):
    for df_gw in dfs_gw:
        if not df_gw[0].empty:
            brush = alt.selection_interval(encodings=['x'])
            fig_base = alt.Chart(df_gw[0]).mark_bar().encode(
                        y='count():Q',
                        color=alt.Color('institution:N', legend=alt.Legend(columns=2, symbolLimit=30)),
                        tooltip=['institution:N','serialNumber:N','testIndex:N','date:T','GW_GLUE_ALL:Q']
            )
            col1 ,col2, col3 = st.columns(3)
            with col1:
                limit_nominal=st.number_input('nominal value', value=0.04380, format='%.5f', step=0.00001, key='gw_limit_n_'+str(df_gw[1]))
            with col2:
                limit_up=st.number_input('upper limit', value=0.04642, format='%.5f', step=0.00001, key='gw_limit_u_'+str(df_gw[1]))
            with col3:
                limit_down=st.number_input('lower limit', value=0.04118, format='%.5f', step=0.00001, key='gw_limit_d_'+str(df_gw[1]))

            line_nominal = alt.Chart(df_gw[0]).mark_rule(color='#EE2C2C', strokeWidth=2
                        ).encode(x=alt.datum(limit_nominal))
            line_up = alt.Chart(df_gw[0]).mark_rule(color='#EE2C2C', strokeDash=[12,6], strokeWidth=2
                        ).encode(x=alt.datum(limit_up))
            line_down = alt.Chart(df_gw[0]).mark_rule(color='#EE2C2C', strokeDash=[12,6], strokeWidth=2
                        ).encode(x=alt.datum(limit_down))

            fig_gw=alt.vconcat(
                fig_base.encode(
                    alt.X('GW_GLUE_ALL:Q',
                    bin=alt.Bin(maxbins=50, extent=brush),
                    scale=alt.Scale(domain=brush),
                    title='Glue Weight (g)'
                    )
                ).properties(
                        height=300,
                        width=600,
                        title="Glue Weight "+str(df_gw[1])
                ),
                (fig_base.encode(
                    alt.X('GW_GLUE_ALL:Q', bin=alt.Bin(maxbins=50),title='Glue Weight (g)'),
                ).add_selection(brush)+line_nominal+line_up+line_down
                ).properties(
                        height=300,
                        width=600,
                        title="Glue Weight "+str(df_gw[1])
                )
            )
            st.altair_chart(fig_gw, use_container_width=False )
    

def GlueWeight(myClient, instCode, compType):
    compSpec={
                "projCode": "S",
                "compTypeCode": compType,
                "filters": {}
            }
    with st.spinner('Getting components...'):
        compInfo=report_tools.GetComponents(myClient, compSpec, instCode=instCode)

    testSpec=[
                {"testCode": 'ASIC_GLUE_WEIGHT' if compType=='HYBRID_ASSEMBLY' else 'GLUE_WEIGHT'} 
                ]
    with st.spinner('Getting test results...'):
        dfs_gw=[]
        compTestRunsInfo=report_tools.GetTestRuns(myClient, compInfo)
        testInfo=report_tools.GetTestRunsDataFiltered(myClient, compTestRunsInfo, testSpec)
        dfTestRuns=report_tools.FormatTestRunData(testInfo)
        testPeriodFilter=report_tools.FilterPeriod(key="tpf_gw"+compType)
        dfTestRuns=dfTestRuns[dfTestRuns['testPeriod']<testPeriodFilter]
        for compSubType in list(dfTestRuns['compSubTypeCode'].unique()):
            dfs_gw.append([FormatTestRunData_GlueWeight(dfTestRuns, compType, compSubType), compSubType])
    
    with st.spinner('Generating Plots...'):
        PlotGlueWeight(dfs_gw)

def FormatTestRunData_GlueHeight(dfTestRuns, compType, compSubType):
    paraCode=''
    moduleParaCodeDict={
            'HYBRID_GLUE_THICKNESS-PB_GLUE_THICKNESS': ['BARREL_LS_MODULE', 'BARREL_SS_MODULE', 'R0', 'R1', 'R2', 'R3M0_HALFMODULE', 'R4M0_HALFMODULE', 'R5M0_HALFMODULE'],
            'HYBRID_GLUE_THICKNESS': ['R3M1_HALFMODULE', 'R4M1_HALFMODULE', 'R5M1_HALFMODULE'],
    }
    if compType=='HYBRID_ASSEMBLY':
        paraCode=['HEIGHT']
    elif compType=='MODULE':
        for k,v in moduleParaCodeDict.items():
            if compSubType in v:
                dfTestRuns['glueHeightCode']=k
                paraCode=k.split('-')
                break
    else:
        st.warning('Unsupported component type for Metrology test!')
        return pd.DataFrame()
    if paraCode=='':
        return pd.DataFrame()

    dfTestRuns=dfTestRuns[dfTestRuns['compSubTypeCode']==compSubType]
    for pc in paraCode:
        dfTestRuns[pc]=dfTestRuns.apply(lambda row: np.mean(np.array([float(v) for v in row['paraValue'].values()])) if row['paraCode']==pc else np.nan, axis=1)
    dfTestRuns_out=dfTestRuns[~dfTestRuns[paraCode[0]].isna()]
    if len(paraCode)>1:
        for pc in paraCode[1:]:
            dfTestRuns_out[pc]=dfTestRuns[~dfTestRuns[pc].isna()][pc]

    dfTestRuns=dfTestRuns.reset_index(drop=True)

    return dfTestRuns


def PlotGlueHeightSub(df_gh, xCode):
    brush = alt.selection_interval(encodings=['x'])
    fig_base = alt.Chart(df_gh[0]).mark_bar().encode(
                y='count():Q',
                color=alt.Color('institution:N', legend=alt.Legend(columns=2, symbolLimit=30)),
                tooltip=['institution:N','serialNumber:N','testIndex:N','date:T',xCode+':Q']
    )
    col1 ,col2, col3 = st.columns(3)
    with col1:
        limit_nominal=st.number_input('nominal value', value=120.0, format='%.1f', step=0.1, key='gw_limit_n_'+str(df_gh[1]))
    with col2:
        limit_up=st.number_input('upper limit', value=160.0, format='%.1f', step=0.1, key='gw_limit_u_'+str(df_gh[1]))
    with col3:
        limit_down=st.number_input('lower limit', value=60.0, format='%.1f', step=0.1, key='gw_limit_d_'+str(df_gh[1]))

    line_nominal = alt.Chart(df_gh[0]).mark_rule(color='#EE2C2C', strokeWidth=2
                ).encode(x=alt.datum(limit_nominal))
    line_up = alt.Chart(df_gh[0]).mark_rule(color='#EE2C2C', strokeDash=[12,6], strokeWidth=2
                ).encode(x=alt.datum(limit_up))
    line_down = alt.Chart(df_gh[0]).mark_rule(color='#EE2C2C', strokeDash=[12,6], strokeWidth=2
                ).encode(x=alt.datum(limit_down))

    fig_gh=alt.vconcat(
        fig_base.encode(
            x=alt.X(xCode+':Q',
            bin=alt.Bin(maxbins=50, extent=brush),
            scale=alt.Scale(domain=brush),
            title=xCode+' (micron)'
            )
        ).properties(
                height=300,
                width=600,
                title="Glue Height "+str(df_gh[1])
        ),
        (fig_base.encode(
            x=alt.X(xCode+':Q', bin=alt.Bin(maxbins=50), title=xCode+' (micron)'),
        ).add_selection(brush)+line_nominal+line_up+line_down).properties(
                height=300,
                width=600,
                title="Glue Height "+str(df_gh[1])
        )
    )

    return fig_gh

def PlotGlueHeight(dfs_gh):
    for df_gh in dfs_gh:
        if not df_gh[0].empty:
            if df_gh[0].loc[0, 'compTypeCode']=='MODULE':
                if df_gh[0].loc[0, 'glueHeightCode']=='HYBRID_GLUE_THICKNESS-PB_GLUE_THICKNESS':
                    st.altair_chart(PlotGlueHeightSub(df_gh, 'HYBRID_GLUE_THICKNESS'), use_container_width=False )
                    st.altair_chart(PlotGlueHeightSub(df_gh, 'PB_GLUE_THICKNESS'), use_container_width=False )
                else:
                    st.altair_chart(PlotGlueHeightSub(df_gh, 'HYBRID_GLUE_THICKNESS'), use_container_width=False )
            else:
                st.altair_chart(PlotGlueHeightSub(df_gh, 'HEIGHT'), use_container_width=False )

def GlueHeight(myClient, instCode, compType):
    compSpec={
                "projCode": "S",
                "compTypeCode": compType,
                "filters": {}
            }
    with st.spinner('Getting components...'):
        compInfo=report_tools.GetComponents(myClient, compSpec, instCode=instCode)

    testSpec=[
                {"testCode": 'ASIC_METROLOGY' if compType=='HYBRID_ASSEMBLY' else 'MODULE_METROLOGY'}
                ]
    with st.spinner('Getting test results...'):
        dfs_gh=[]
        compTestRunsInfo=report_tools.GetTestRuns(myClient, compInfo)
        testInfo=report_tools.GetTestRunsDataFiltered(myClient, compTestRunsInfo, testSpec)
        dfTestRuns=report_tools.FormatTestRunData(testInfo)
        testPeriodFilter=report_tools.FilterPeriod(key="tpf_gh"+compType)
        dfTestRuns=dfTestRuns[dfTestRuns['testPeriod']<testPeriodFilter]
        for compSubType in list(dfTestRuns['compSubTypeCode'].unique()):
            dfs_gh.append([FormatTestRunData_GlueHeight(dfTestRuns, compType, compSubType), compSubType])
    
    with st.spinner('Generating Plots...'):
        PlotGlueHeight(dfs_gh)

### old, only for IV consistency, table highly customized, not using itk-report's functions, could&need to be improved
def GetTestRunsByTestType_df(client, compType, testType):
    list_TestRuns = client.get("listTestRunsByTestType",json={"project":"S","componentType":str(compType),"testType":str(testType),"pageInfo":{"pageSize": 4000}})
    return pd.DataFrame(list_TestRuns)

def get_ComponentListByType_df(client, compType, subType, site):
    list_Components = client.get('listComponents',json={'filterMap':{'project':'S', 'componentType': [compType], 'type': [subType], 'institute': [site]}, "pageInfo":{"pageSize": 4000}})
    return pd.DataFrame(list_Components)

@st.cache_data(show_spinner=False)
def GetTestRunsByTestTypeAndSite_df(_client, compType, testType, site):
    num_bulk = 1000
    list_TestRunBulkFiltered = []

    df_TestRuns = GetTestRunsByTestType_df(_client, compType, testType)
    df_TestRunsSiteFiltered = df_TestRuns[pd.DataFrame(list(df_TestRuns["institution"]))["code"]==str(site)]
    list_id = list(df_TestRunsSiteFiltered.loc[:,"id"])

    num_cycle = len(list_id)//num_bulk
    remainder = len(list_id)%num_bulk
    for i in range(num_cycle):
        list_TestRunBulkFiltered.extend(_client.get("getTestRunBulk", json={"testRun":list_id[0:(i+1)*num_bulk]}))
    if remainder != 0:
        list_TestRunBulkFiltered.extend(_client.get("getTestRunBulk", json={"testRun":list_id[num_cycle*num_bulk : remainder ]}))
    
    return pd.DataFrame(list_TestRunBulkFiltered)

@st.cache_data(show_spinner=False)
def GetTestRunsResults_list(_client, compType, testType, site):
    df_TestRunsFiltered = GetTestRunsByTestTypeAndSite_df(_client, compType, testType, site)
    ret=[]
    try:
        ret = list(df_TestRunsFiltered["results"]) # df_TestRunsFiltered["results"] will return a pd.Series object
    except KeyError:
        st.error("No requested testRun type for site \"" + site +"\"")

    return ret  

def sensorIVConsistency(client):
    result_df = pd.DataFrame(columns=['Sensor SN', 'Sensor VBD', 'Sensor I_500V', 
                                      'Sensor IV Test Institution', 'Module SN', 
                                      'Module VBD', 'Module I_500V', 'Module IV Test Institution'])
    
    SENSOR_TYPE_CODES = ('ATLAS18LS', 'ATLAS18SS', 'ATLAS18R0','ATLAS18R1','ATLAS18R2','ATLAS18R3','ATLAS18R4','ATLAS18R5')
    SENSOR_HOME_SITES = ('CERN', 'KEK')
    ready_stage = {'code': 'READY_FOR_MODULE', 'name': 'Ready for module', 'order': 4, 'initial': False, 'final': True}

    sub_type = st.selectbox('Choose a sensor type', ('',)+SENSOR_TYPE_CODES)
    sites = st.selectbox('Choose a sensor home site', ('',)+SENSOR_HOME_SITES)
    if sub_type != '' and sites != '':
        sensor_df = get_ComponentListByType_df(client, 'SENSOR', sub_type, sites)
        if not sensor_df.empty:
            # filter sensors in a hardcoded way
            sensor_sn_list = sensor_df[sensor_df['currentStage']==ready_stage]['serialNumber'].to_list() 
            
            chunk=100
            foundList=[]
            for x in range(0,int(np.ceil(len(sensor_sn_list)/chunk))):
                foundList.extend(client.get('getComponentBulk', json={'component': sensor_sn_list[x*chunk:(x+1)*chunk] }) )
            sensor_full_df = pd.DataFrame(foundList)
            
            # filter out sensors that are not assembled to a module
            filter_list = [False]*len(sensor_full_df)
            module_sn_list = []
            for index, row in sensor_full_df.iterrows():
                for parent in row['parents']:
                    if parent['componentType']['code'] == 'MODULE' and parent['component'] is not None:
                        filter_list[index] = True
                        module_sn_list.append(parent['component']['serialNumber'])

            sensor_assembled_df = sensor_full_df[filter_list].reset_index(drop=True)
            sensor_assembled_sn_list = sensor_assembled_df['serialNumber'].to_list()

            # get id of the last sensor IV test
            sensor_IVTest_id_list = []
            no_test_index_list = []
            for index, row in sensor_assembled_df.iterrows():
                id_get = False
                for test in row['tests']:
                    if test['code'] == 'ATLAS18_IV_TEST_V1':
                        id_get = True
                        # use 'cts' instead of 'date' to avoid parsing non standard time, might cause problem
                        date = datetime.datetime.fromisoformat(test['testRuns'][0]['cts'].rstrip('Z')) 
                        run_idx = 0
                        idx = 0
                        # get the idx of the last IV testrun
                        for testRun in test['testRuns']:
                            this_run_date = datetime.datetime.fromisoformat(testRun['cts'].rstrip('Z')) 
                            run_idx += 1
                            if this_run_date > date:
                                date = this_run_date
                                idx = run_idx-1
                        sensor_IVTest_id_list.append(test['testRuns'][idx]['id'])
                if not id_get:
                    sensor_IVTest_id_list.append('')
                    no_test_index_list.append(index)

            print(sensor_IVTest_id_list)
            for i in range(len(no_test_index_list)-1, -1, -1):
                sensor_IVTest_id_list.pop(no_test_index_list[i])
                sensor_assembled_sn_list.pop(no_test_index_list[i])
                module_sn_list.pop(no_test_index_list[i])
            
            chunk=100
            foundList=[]
            for x in range(0,int(np.ceil(len(sensor_IVTest_id_list)/chunk))):
                foundList.extend(client.get('getTestRunBulk', json={'testRun': sensor_IVTest_id_list[x*chunk:(x+1)*chunk] }) )
            sensor_IVTest_df = pd.DataFrame(foundList)
            if sensor_IVTest_df.empty:
                st.dataframe(result_df)
                return
            sensor_IVTest_df = report_tools.sortDataFrameByList(sensor_IVTest_df, 'id', sensor_IVTest_id_list)

            # get IV test parameters
            sensor_IVTest_institution_list = []
            sensor_VBD_list = []
            sensor_I_500V_list = []
            for index, row in sensor_IVTest_df.iterrows():
                sensor_IVTest_institution_list.append(row['institution']['code'])
                for result in row['results']:
                    if result['code'] == 'VBD':
                        sensor_VBD_list.append(result['value'])
                    if result['code'] == 'I_500V':
                        sensor_I_500V_list.append(result['value'])

            ## extract post-tabbing parameters from corresponding module 
            chunk=100
            foundList=[]
            for x in range(0,int(np.ceil(len(module_sn_list)/chunk))):
                foundList.extend(client.get('getComponentBulk', json={'component': module_sn_list[x*chunk:(x+1)*chunk] }) )
            module_assembled_df = pd.DataFrame(foundList)
            module_assembled_df = report_tools.sortDataFrameByList(module_assembled_df,'serialNumber',module_sn_list)

            # get id of the last sensor IV test
            module_IVTest_id_list = []
            no_test_index_list = []
            for index, row in module_assembled_df.iterrows():
                id_get = False
                for test in row['tests']:
                    if test['code'] == 'MODULE_IV_PS_V1':
                        testRuns = []
                        for testRun in test['testRuns']:
                            if testRun['institution']['code'] == sensor_IVTest_institution_list[index]:
                                id_get = True
                                testRuns.append(testRun)
                        if len(testRuns) > 0:
                            date = datetime.datetime.fromisoformat(testRuns[0]['cts'].rstrip('Z')) 
                            run_idx = 0
                            idx = 0
                            for testRun in testRuns:
                                this_run_date = datetime.datetime.fromisoformat(testRun['cts'].rstrip('Z'))
                                run_idx += 1
                                if this_run_date > date:
                                    date = this_run_date
                                    idx = run_idx-1
                            module_IVTest_id_list.append(testRuns[idx]['id'])
                if not id_get:
                    module_IVTest_id_list.append('')
                    no_test_index_list.append(index)

            for i in range(len(no_test_index_list)-1, -1, -1):
                module_sn_list.pop(no_test_index_list[i])
                module_IVTest_id_list.pop(no_test_index_list[i])
                sensor_assembled_sn_list.pop(no_test_index_list[i])
                sensor_IVTest_institution_list.pop(no_test_index_list[i])
                sensor_VBD_list.pop(no_test_index_list[i])
                sensor_I_500V_list.pop(no_test_index_list[i])

            chunk=100
            foundList=[]
            for x in range(0,int(np.ceil(len(module_IVTest_id_list)/chunk))):
                foundList.extend(client.get('getTestRunBulk', json={'testRun': module_IVTest_id_list[x*chunk:(x+1)*chunk] }) )
            module_IVTest_df = pd.DataFrame(foundList)
            if module_IVTest_df.empty:
                st.dataframe(result_df)
                return
            module_IVTest_df = report_tools.sortDataFrameByList(module_IVTest_df, 'id', module_IVTest_id_list)

            # get IV test parameters
            module_IVTest_institution_list = []
            module_VBD_list = []
            module_I_500V_list = []
            for index, row in module_IVTest_df.iterrows():
                module_IVTest_institution_list.append(row['institution']['code'])
                for result in row['results']:
                    if result['code'] == 'VBD':
                        module_VBD_list.append(result['value'])
                    if result['code'] == 'I_500V':
                        module_I_500V_list.append(result['value'])

            result_df = pd.DataFrame({'Sensor SN': sensor_assembled_sn_list,
                                        'Sensor VBD': sensor_VBD_list,
                                        'Sensor I_500V': sensor_I_500V_list,
                                        'Sensor IV Test Institution': sensor_IVTest_institution_list,
                                        'Module SN': module_sn_list,
                                        'Module VBD': module_VBD_list,
                                        'Module I_500V': module_I_500V_list,
                                        'Module IV Test Institution': module_IVTest_institution_list
            })

            st.dataframe(result_df)

def countModuleBonding(row):
    nRows=4
    failed_FEs=[[]]*nRows
    repaired_FEs=[[]]*nRows
    failed_H2PB=[]
    repaired_H2PB=[]

    for parameter in row['results']:
        for i in range(nRows):
            ### failed FE
            if parameter['code']=='FAILED_FRONTEND_ROW'+str(i+1):
                if type(parameter['value']) == dict: ###cannot to count wires from error type (int...)
                    for key in parameter['value']:
                        if '-' in key:
                            splitedKey = key.split('-')
                            failed_FEs[i].extend([k for k in range(int(splitedKey[0].strip('XY')), int(splitedKey[1].strip('XY'))+1)])
                        elif key != '':
                            failed_FEs[i].append(key)
            ### repaired FE
            if parameter['code']=='REPAIRED_FRONTEND_ROW'+str(i+1):
                if type(parameter['value']) == dict:
                    for key in parameter['value']:
                        if '-' in key:
                            splitedKey = key.split('-')
                            repaired_FEs[i].extend([k for k in range(int(splitedKey[0].strip('XY')), int(splitedKey[1].strip('XY'))+1)])
                        elif key != '':
                            repaired_FEs[i].append(key)

        #FAILED_HYBRID_TO_PB
        if parameter['code']=='FAILED_HYBRID_TO_PB':
            if type(parameter['value']) == dict:
                for key in parameter['value']:
                    if 'P-H' in key:
                        failed_H2PB.append(key)
                    elif '-' in key and 'P' not in key:
                        splitedKey = key.split('-')
                        failed_H2PB.extend([k for k in range(int(splitedKey[0].strip('XY')), int(splitedKey[1].strip('XY'))+1)])
                    elif key != '':
                        failed_H2PB.append(key)

        #REPAIRED_HYBRID_TO_PB
        if parameter['code']=='REPAIRED_HYBRID_TO_PB':
            if type(parameter['value']) == dict:
                for key in parameter['value']:
                    if 'P-H' in key:
                        repaired_H2PB.append(key)
                    elif '-' in key and 'P' not in key:
                        splitedKey = key.split('-')
                        repaired_H2PB.extend([k for k in range(int(splitedKey[0].strip('XY')), int(splitedKey[1].strip('XY'))+1)])
                    elif key != '':
                        repaired_H2PB.append(key)

    for i in range(nRows):
        failed_FEs[i] = [k for k in failed_FEs[i] if k not in repaired_FEs[i]] ### to remove failedBUTrepaied bonds

    failed_H2PB = [k for k in failed_H2PB if k not in repaired_H2PB]

    failed_FE_count = sum(len(row) for row in failed_FEs)
    repaired_FE_count = sum(len(row) for row in repaired_FEs)
    failed_H2PB_count = len(failed_H2PB)
    repaired_H2PB_count = len(repaired_H2PB)

    return pd.Series([failed_FE_count, repaired_FE_count, failed_H2PB_count, repaired_H2PB_count])

def countHybridBonding(row):
    failed_ASIC_BE=[]
    repaired_ASIC_BE=[]
    
    for parameter in row['results']:
        # FAILED_ASIC_BACKEND
        if parameter['code']=='FAILED_ASIC_BACKEND':
            if type(parameter['value']) == dict:
                for key in parameter['value']:
                    if '-' in key:
                        splitedKey = key.split('-')
                        failed_ASIC_BE.extend([k for k in range(int(splitedKey[0].strip('XY')), int(splitedKey[1].strip('XY'))+1)])
                    elif key != '':
                        failed_ASIC_BE.append(key)
        # REPAIRED_ASIC_BACKEND 
        if parameter['code']=='REPAIRED_ASIC_BACKEND':
            if type(parameter['value']) == dict:
                for key in parameter['value']:
                    if '-' in key:
                        splitedKey = key.split('-')
                        repaired_ASIC_BE.extend([k for k in range(int(splitedKey[0].strip('XY')), int(splitedKey[1].strip('XY'))+1)])
                    elif key != '':
                        repaired_ASIC_BE.append(key)

    failed_ASIC_BE = [k for k in failed_ASIC_BE if k not in repaired_ASIC_BE]
    failed_ASIC_BE_count = len(failed_ASIC_BE)
    repaired_ASIC_BE_count = len(repaired_ASIC_BE)

    return pd.Series([failed_ASIC_BE_count, repaired_ASIC_BE_count])

def FormatTestRunData_WireBonding(dfTestRuns):
    dfTestRuns=dfTestRuns[['serialNumber', 'date', 'institution', 'compTypeCode', 'compSubTypeCode', 'passedOrNot', 'results']]

    if dfTestRuns.loc[0,'compTypeCode'] == 'HYBRID_ASSEMBLY':
        dfTestRuns[['FailedBE', 'RepairedBE']] = dfTestRuns.apply(countHybridBonding, axis=1)
    elif dfTestRuns.loc[0,'compTypeCode'] == 'MODULE':
        dfTestRuns[['FailedFE', 'RepairedFE', 'FailedH2PB', 'RepairedH2PB']] = dfTestRuns.apply(countModuleBonding, axis=1)
    
    dfTestRuns = dfTestRuns.drop(columns=['results'])
    dfTestRuns['passedOrNot'] = dfTestRuns.pop('passedOrNot')

    dfTestRuns = dfTestRuns.rename(columns={'serialNumber':'SerialNumber', 'date':'Date', 'institution':'Institution', 'compTypeCode':'ComponenType', 'compSubTypeCode':'Type', 'passedOrNot':'PassedOrNot'})

    return dfTestRuns

def WireBonding(myClient, instCode, compType):
    compSpec={
                "projCode": "S",
                "compTypeCode": compType,
                "filters": {}
            }
    with st.spinner('Getting components...'):
        compInfo=report_tools.GetComponents(myClient, compSpec, instCode=instCode)

    testSpec=[
                {"testCode": 'WIRE_BONDING' if compType=='HYBRID_ASSEMBLY' else 'MODULE_WIRE_BONDING'}
                ]
    with st.spinner('Getting test results...'):
        dfs_wb=[]
        compTestRunsInfo=report_tools.GetTestRuns(myClient, compInfo)
        testInfo=report_tools.GetTestRunsDataFiltered(myClient, compTestRunsInfo, testSpec)
        dfTestRuns=report_tools.FormatTestRunDataII(testInfo)
        for compSubType in list(dfTestRuns['compSubTypeCode'].unique()):
            st.write(compSubType)
            dfTestRunsSub=dfTestRuns[dfTestRuns['compSubTypeCode']==compSubType]
            if dfTestRunsSub.empty:
                st.dataframe(pd.DataFrame())
                print(compSubType)
                continue
            dfTestRunsSub=dfTestRunsSub.loc[dfTestRunsSub.groupby('serialNumber')['date'].idxmax()].reset_index(drop=True)
            testPeriodFilter=report_tools.FilterPeriod(key="tpf_wb"+compType+compSubType)
            dfTestRunsSub=dfTestRunsSub[dfTestRunsSub['testPeriod']<testPeriodFilter]
            st.dataframe(FormatTestRunData_WireBonding(dfTestRunsSub))

####################
### Hybrid Noise ###
####################

#bespoke formatting based on generic formatting
def FormatTestRunData_hybrid_noise(df_testRuns, testStage):
    df_sub=df_testRuns.query('stage=="'+testStage+'"')

    dfs_out=[]
    paraCodes=['innse_away', 'innse_under']
    for i in range(len(paraCodes)):
        dfs_out.append(df_sub.query('paraCode=="'+paraCodes[i]+'"').reset_index())
        dfs_out[i]["paraValue"]=dfs_out[i]['paraValue'].apply(lambda x: [nse for chip in x for nse in chip])
        dfs_out[i]["paraValueMax"]=dfs_out[i]['paraValue'].apply(lambda x: max(x))
        dfs_out[i]["channelNo"]=dfs_out[i]['paraValue'].apply(lambda x: list(range(len(x))))
        dfs_out[i]=dfs_out[i].explode(['paraValue', 'channelNo'])
        dfs_out[i]=dfs_out[i][['id', 'serialNumber', 'date', 'institution', 'testIndex','compTypeCode', 'paraValue', 'paraValueMax', 'channelNo']]
        # dfs_out[i].reset_index(drop=True, inplace=True)

    return dfs_out

def plot_hybrid_noise(df_innse, stage, fig_title):
    limit_nominal=st.number_input('nominal value', value=1200.0, format='%.1f', step=0.1, key='noise_limit_n_'+str(stage)+str(fig_title))
    line_nominal = alt.Chart(df_innse).mark_rule(color='#EE2C2C', strokeWidth=3, strokeDash=[12,12]
                        ).encode(y=alt.datum(limit_nominal))

    fig_innse_base = (alt.Chart(df_innse).mark_line(point=alt.OverlayMarkDef(size=10)).encode(
                x=alt.X('channelNo:Q', title="Channel"),
                shape=alt.Shape('institution:N', legend=alt.Legend(columns=3, symbolLimit=0, title='Tested At')),
                color=alt.Color('serialNumber:N', legend=alt.Legend(columns=3, symbolLimit=36, title='Serial Number')),
                strokeDash=alt.StrokeDash('testIndex:N', legend=None),
                detail=alt.Detail('date:T'),
                tooltip=['id:N','institution:N','serialNumber:N','testIndex:N','date:T','channelNo:Q','paraValue:Q']
    )+line_nominal).properties(
                height=300,
                width=600, ### will be overwrited if set 'use_container_width=True' for st.altair_chart
                title=fig_title 
    )

    sn_dropdown = alt.binding_select(options=[None]+list(df_innse['serialNumber'].unique()), 
                                          labels=['All']+list(df_innse['serialNumber'].unique()),
                                          name='Serial Number')
    sn_selection = alt.selection_single(fields=['serialNumber'], bind=sn_dropdown)
    fig_innse=fig_innse_base.encode(
                y=alt.Y('paraValue:Q', title="Noise")
    ).add_selection(sn_selection).transform_filter(sn_selection).interactive(bind_x=False)
    st.altair_chart(fig_innse, use_container_width=True)

    y_max_value=max(list(df_innse['paraValueMax']))
    y_max=st.number_input("Set Y-maximum of the figure below", min_value=0, max_value=int(y_max_value+1000), 
                    value=2000, step=500, key='y_max_noise_'+str(stage)+str(fig_title))

    sn_dropdown_normal = alt.binding_select(options=[None]+list(df_innse[df_innse['paraValueMax']<y_max]['serialNumber'].unique()), 
                                          labels=['All']+list(df_innse[df_innse['paraValueMax']<y_max]['serialNumber'].unique()),
                                          name='Serial Number')
    sn_selection_normal = alt.selection_single(fields=['serialNumber'], bind=sn_dropdown)
    fig_innse_normal=fig_innse_base.encode(
                y=alt.Y('paraValue:Q', title="Noise", scale=alt.Scale(domain=[-100, y_max]))
    ).transform_filter(alt.datum.paraValueMax < y_max
    ).add_selection(sn_selection_normal).transform_filter(sn_selection_normal).interactive()

    st.altair_chart(fig_innse_normal, use_container_width=True)


def hybrid_noise(myClient, instCode):
    compSpec={
                "projCode": "S",
                "compTypeCode": "HYBRID_ASSEMBLY",
                "filters": {}
                }
    with st.spinner('Getting components...'):
        compInfo=report_tools.GetComponents(myClient, compSpec, instCode=instCode)

    testSpec=[
                {"testCode":"RESPONSE_CURVE_PPA"} 
            ]
    with st.spinner('Getting test results...'):
        compTestRunsInfo=report_tools.GetTestRuns(myClient, compInfo)
        testInfo=report_tools.GetTestRunsDataFiltered(myClient, compTestRunsInfo, testSpec)
        dfTestRuns=report_tools.FormatTestRunData(testInfo)
        testPeriodFilter=report_tools.FilterPeriod(key="tpf_hyb_noise")
        dfTestRuns=dfTestRuns[dfTestRuns['testPeriod']<testPeriodFilter]
        stage=st.selectbox(label='Choose stages in which tests are performed', options=[' ']+list(dfTestRuns.stage.unique()))
        if stage != ' ':
            dfs_noise = FormatTestRunData_hybrid_noise(dfTestRuns, stage)
        else:
            st.stop()

    with st.spinner('Generating Plots...'):
        titles=["Input Noise-Away", "Input Noise-Under"]
        for i in range(len(titles)):
            plot_hybrid_noise(dfs_noise[i], stage, titles[i])

###################
### QC Overview ###
###################

def FormatTestRunData_QCOverview(dfTestRuns):
    dfTestRuns['compCodeStageTestCode'] = dfTestRuns.apply(lambda row: '-'.join([row['compCode'], row['stage'],str(row['testCode'])]), axis=1)
    dfTestRunsGrouped=dfTestRuns.groupby(by=['compCode', 'stage', 'testCode'], sort=False, group_keys=True)
    dfTestRunsGrouped=dfTestRunsGrouped.apply(lambda x: x.assign(passNum=sum(list(x['passed'])), failNum=(len(list(x['passed']))-sum(list(x['passed'])))))
    dfTestRuns = dfTestRunsGrouped.reset_index(drop=True)

    return dfTestRuns

def QCOverviewTable(dfTestRuns, dfCompTestTypes):
    df_qc=dfCompTestTypes.pivot(columns=['stage','testCode'], values='stageAndTestCode')
    df_qc=df_qc.drop(df_qc.index)
    for col in ['compSubTypeCode', 'localName', 'alternativeIdentifier', 'serialNumber', 'compCode']:
        df_qc.insert(0, col, list(dfTestRuns.loc[~dfTestRuns['compCode'].duplicated(keep='first'), col]) )
    df_qc.set_index('compCode', inplace=True)
    df_qc.fillna(' ', inplace=True)

    df_sub=pd.DataFrame({'compCodeStageTestCode': list(dfTestRuns['compCodeStageTestCode'].unique())})
    df_sub['passNum']=list(dfTestRuns.loc[~dfTestRuns['compCodeStageTestCode'].duplicated(keep='first'), 'passNum'])
    df_sub['failNum']=list(dfTestRuns.loc[~dfTestRuns['compCodeStageTestCode'].duplicated(keep='first'), 'failNum'])
    df_sub.set_index('compCodeStageTestCode',inplace=True)

    for cst in df_sub.index.tolist():
        csts=cst.split('-')
        if '-'.join(csts[1:]) in list(dfCompTestTypes['stageAndTestCode']):
            df_qc.loc[csts[0], (csts[1],csts[2])]='PASS '+str(df_sub.loc[cst,"passNum"])+' | FAIL '+str(df_sub.loc[cst,"failNum"])
    df_qc.reset_index(drop=True,inplace=True)

    return df_qc

def QCOverview(myClient, instCode, compType):
    compSpec={
                "projCode": "S",
                "compTypeCode": compType,
                "filters": {}
            }
    with st.spinner('Getting components...'):
        compInfo=report_tools.GetComponents(myClient, compSpec, instCode=instCode)
    
    with st.spinner('Getting test results...'):
        compTestRunsInfo=report_tools.GetTestRuns(myClient, compInfo)
        testInfo=report_tools.GetTestRunsDataFiltered(myClient, compTestRunsInfo, "all")
        dfTestRuns=report_tools.FormatTestRunData(testInfo)
        dfTestRuns=FormatTestRunData_QCOverview(dfTestRuns)
        dfCompTestTypes=report_tools.GetComponentTestTypes(myClient, compSpec['compTypeCode'])

    with st.spinner('Generating QC Overview table...'):
        df_qc = QCOverviewTable(dfTestRuns, dfCompTestTypes)
        df_qc_s = df_qc.style.applymap(lambda v: 'color:white;background-color:darkblue' if v !=' ' and 'PASS' in v else None)
    st.dataframe(df_qc_s)

#################
### Inventory ###
#################

def CountInventory(df_comps):
    compTypes=['HCC', 'ABC', 'HYBRID_FLEX', 'HYBRID_ASSEMBLY', 'PWB', 'SENSOR']
    
    dfs={}
    for compType in compTypes:
        dfs[compType]=df_comps[df_comps['componentType']==compType]

    def remove_and_return(lst, element):
        if element in lst:
            lst.remove(element)
        return lst

    dfs['HCC']['available']=dfs['HCC']['parentTypes'].apply(lambda x: len(remove_and_return(x,'HCC_GELPACK'))==0 )
    dfs['ABC']['available']=dfs['ABC']['parentTypes'].apply(lambda x: len(remove_and_return(x,'ABC_GELPACK'))==0 )
    dfs['HYBRID_FLEX']['available']=dfs['HYBRID_FLEX']['parentTypes'].apply(lambda x: len(remove_and_return(x,'HYBRID_FLEX_ARRAY'))==0 )
    dfs['HYBRID_ASSEMBLY']['available']=dfs['HYBRID_ASSEMBLY']['parentTypes'].apply(lambda x: len(remove_and_return(x,'HYBRID_TEST_PANEL'))==0 )
    dfs['PWB']['available']=dfs['PWB']['parentTypes'].apply(lambda x: 'MODULE' not in x )
    dfs['SENSOR']['available']=dfs['SENSOR']['parentTypes'].apply(lambda x: 'MODULE' not in x )
        
    df_inventory_ava=pd.DataFrame({'compType':compTypes, 'counts':[sum(list(dfs[ct]['available'])) for ct in compTypes]})
    df_inventory=pd.DataFrame({'compType':compTypes, 'counts':[len(dfs[ct]) for ct in compTypes]})

    return df_inventory_ava, df_inventory

def PlotInventory(df_inventory_ava, df_inventory):
    fig_inventory_ava = alt.Chart(df_inventory_ava).mark_bar(opacity=0.8).encode(
                x=alt.X('compType:N', title="Component Type"),
                y=alt.Y('counts:Q', title="Counts"),
                color=alt.Color('compType:N'),
                shape=alt.Shape('institution:N', legend=alt.Legend(columns=3, symbolLimit=0, title='Institution')),
                tooltip=['compType:N', 'counts:Q','institution:N']
    )

    fig_inventory = alt.Chart(df_inventory).mark_bar(color='#CCCCCC').encode(
                x=alt.X('compType:N', title="Component Type"),
                y=alt.Y('counts:Q', title="Counts"),
                shape=alt.Shape('institution:N', legend=alt.Legend(columns=3, symbolLimit=0, title='Institution')),
                tooltip=['compType:N', 'counts:Q', 'institution:N']
    )

    fig= (fig_inventory+fig_inventory_ava).properties(
                height=450,
                width=600,
                title="Inventory"
    ).interactive(bind_x=False)

    return fig


def Inventory(myClient, instCode):
    compSpec={
                "projCode": "S",
                "compTypeCode": "all",
                "filters": {}
            }
    with st.spinner('Getting components...'):
        compInfo=report_tools.GetComponents(myClient, compSpec, instCode=instCode)

    with st.spinner('Getting components data...'):
        df_comps=report_tools.GetComponentData(myClient, compInfo, chunk=1000)
        df_comps=report_tools.FormatComponentData(df_comps)

    with st.spinner('Plotting...'):
        dfs_inventory=CountInventory(df_comps)
        fig=PlotInventory(*dfs_inventory) 

    st.altair_chart(fig, use_container_width=True)

######################
### Batch-based QC ###
######################

def BatchQCTable(myClient, foundBatches, compTypeSpec, compTestSpec):
    dfBatchQC=pd.DataFrame()
    dfAllInst=pd.DataFrame(myClient.get('listInstitutions',json={}).data)
    dfAllInst.set_index('id',inplace=True)
    for batchId in foundBatches:
        dfBatchQCSingle=pd.DataFrame()
        batch=myClient.get('getBatch', json={'id': batchId})
        dfBatchQCSingle['Batch ID']=[batch['id']]
        dfBatchQCSingle['Batch Number']=[batch['number']]
        dfBatchQCSingle['Batch Creation Time']=[pd.to_datetime(batch['cts'],format='%Y-%m-%dT%H:%M:%S.%f')]
        ### Get components from the batch to get related testruns
        dfComp=pd.DataFrame(batch['components'])
        dfComp['compTypeCode']=dfComp['componentType'].apply(lambda x: x['code'])
        dfComp=dfComp[dfComp['compTypeCode'].isin([x['compCode'] for x in compTypeSpec])]
        foundComps=[{'code':c} for c in list(dfComp['code'])]
        testRunIDs=report_tools.GetTestRuns(myClient, foundComps)
        testInfo=report_tools.GetTestRunsDataFiltered(myClient, testRunIDs, compTestSpec)
        dfTestRuns=report_tools.FormatTestRunData(testInfo)
        if len(dfTestRuns)==0:
            for tc in set([x['testCode'] for x in compTestSpec]):
                dfBatchQCSingle[tc+' Passed']=[None]
            for pc in set([x['paraCode'] for x in compTestSpec if 'paraCode' in x.keys()]):
                colName=pc
                for x in [spec for spec in compTestSpec if 'paraCode' in spec.keys() ]:
                    if x['paraCode']==pc:
                        colName=x['columnName']
                dfBatchQCSingle[colName+' (batch avg.)']=[None]
            dfBatchQCSingle['Owner Institutes']=[[dfAllInst.loc[instId,'code'] for instId in batch['ownerInstituteList']]]
            dfBatchQC=pd.concat([dfBatchQC, dfBatchQCSingle])
            continue
        ### testCode part (at least one passed)
        dfTestRunsGbyTestCode=dfTestRuns.groupby(by=['testCode'])
        for tc in set([x['testCode'] for x in compTestSpec]):
            if tc in dfTestRunsGbyTestCode.groups.keys():
                data=dfTestRunsGbyTestCode.get_group(tc)
                dfBatchQCSingle[tc+' Passed']=[True] if (data['passed'].sum())>0 else [False]
            else:
                dfBatchQCSingle[tc+' Passed']=[None]
        ### parameter part
        dfTestRuns=dfTestRuns[(dfTestRuns['paraCode'].isin([x['paraCode'] for x in compTestSpec]))]
        dfTestRunsGbyParaCode=dfTestRuns.groupby(by=['paraCode'])
        for pc in set([x['paraCode'] for x in compTestSpec]):
            if pc in dfTestRunsGbyParaCode.groups.keys():
                data=dfTestRunsGbyParaCode.get_group(pc)
                colName=pc
                for x in compTestSpec:
                    if x['paraCode']==pc:
                        colName=x['columnName']
                dfBatchQCSingle[colName+' (batch avg.)']=[data['paraValue'].mean()]
            else:
                dfBatchQCSingle[pc+' (batch avg.)']=[None]
        dfBatchQCSingle['Owner Institutes']=[[dfAllInst.loc[instId,'code'] for instId in batch['ownerInstituteList']]]
        dfBatchQC=pd.concat([dfBatchQC, dfBatchQCSingle])

    if len(dfBatchQC)>0:
        dfBatchQC.sort_values(by='Batch Creation Time', inplace=True, ascending=False)
    
    dfBatchQC.reset_index(drop=True,inplace=True)
        
    return dfBatchQC

def BatchQC(myClient, instCode, batchSpec, entryLimit=10):
    foundBatches=report_tools.GetBatchIDs(myClient, instCode, batchSpec['batchType'], entryLimit)
    dfBatchQC=BatchQCTable(myClient, foundBatches, batchSpec['compTypeSpec'], batchSpec['compTestSpec'])
    st.dataframe(dfBatchQC)

#######################
### Report Selector ###
#######################

def HybridAssemblyReports(client, sites, comp_type, test_type):
    if test_type == "ASIC_GLUE_HEIGHT":
        GlueHeight(client, sites, comp_type)
    if test_type=="ASIC_GLUE_WEIGHT":
        GlueWeight(client, sites, comp_type)
    if test_type=="INPUT_NOISE":
        hybrid_noise(client, sites)
    if test_type=="WIRE_BONDING":
        WireBonding(client, sites, comp_type)

def ModuleTestReports(client, sites, comp_type, test_type):
    if test_type == "GLUE_HEIGHT":
        GlueHeight(client, sites, comp_type)
    if test_type=="GLUE_WEIGHT":
        GlueWeight(client, sites, comp_type)
    if test_type=="WIRE_BONDING":
        WireBonding(client, sites, comp_type)
    if test_type=="IV_TEST_CONSISTENCY":
        sensorIVConsistency(client)

#####################
### main part
#####################

class Page4(Page):
    def __init__(self):
        super().__init__("PDB Report", ":bookmark_tabs: Production Database Report", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]
        
        nowTime = datetime.datetime.now(datetime.timezone(datetime.timedelta(hours=8))) #UTC+8 Beijing Time

        if "myClient" not in list(st.session_state.keys()):
            st.header(':ticket: Get PDB token')

            infra.TextBox(pageDict, 'ac1', 'Enter password 1', True)
            infra.TextBox(pageDict, 'ac2', 'Enter password 2', True)

            if st.session_state.debug:
                st.write("**DEBUG** tokens")
                st.write("ac1:",pageDict['ac1'],", ac2:",pageDict['ac2'])

            if st.button("Get Token"):
                pageDict['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
                st.session_state.myClient=DBaccess.AuthenticateUser(pageDict['ac1'],pageDict['ac2'])

            try:
                st.write("###### Registed at",pageDict['time'])
                exTime = datetime.datetime.fromtimestamp(st.session_state.myClient.user.expires_at, datetime.timezone(datetime.timedelta(hours=8)))
                st.write("###### Token expires at: "+exTime.strftime("%Y-%m-%d  %H:%M"))
                
                try:
                    pageDict['user']=st.session_state.myClient.get('getUser', json={'userIdentity': st.session_state.myClient.user.identity})
                    st.success("Returning token for "+str(pageDict['user']['firstName'])+" "+str(pageDict['user']['lastName'])+" seems ok.")
                    if st.session_state.debug:
                        st.write("User (id):",pageDict['user']['firstName'],pageDict['user']['lastName'],"("+pageDict['user']['id']+")")
                    with st.spinner("Refreshing..."):
                        time.sleep(2)
                        st.experimental_rerun()
                except BadRequest: 
                    st.error("Bad token registered. Please close streamlit and try again")
            except KeyError:
                st.info("No token yet registed")
            
            st.write("---")

        ### check requirements to do stuff
        doWork=False
        try:
            if st.session_state.myClient:
                doWork=True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.warning("No token, please authenticate first ")

        ### gatekeeping
        if not doWork:
            st.stop()


        ### Reports
        client = st.session_state.myClient
        
        alt.data_transformers.enable('default', max_rows=10000000)
        st.header("Test Parameters")
        compType=st.selectbox("Choose a component type", ('',)+tuple(testTypes.keys()) )
        testType=""
        sites=[]
        if compType != "":
            testType=st.selectbox("Choose a test type", ('',)+tuple(testTypes[compType]))
        if testType != "" and testType!='IV_TEST_CONSISTENCY': ###FIXME: change to | if testType != "" | after optimizing the IVconsistency 
            scope=st.radio("Choose the scope of reporting", ['Site-wide', 'Cluster-wide', 'All'], horizontal=True, key='rpt_scope_tp')
            if scope=='Site-wide':
                clusCode=st.selectbox("Choose a strip cluster", ['',]+report_tools.STRIP_CLUSTERS )
                if clusCode != "":
                    with st.spinner('Getting clutser institutes...'):
                        clusSites=report_tools.GetClusterInstitutes(client, clusCode)
                    sites=st.multiselect("Choose institutes (component current location)", clusSites)
            if scope=='Cluster-wide':
                clusCode=st.selectbox("Choose a strip cluster", ['',]+report_tools.STRIP_CLUSTERS)
                if clusCode != "":
                    with st.spinner('Getting clutser institutes...'):
                        sites=report_tools.GetClusterInstitutes(client, clusCode)
            if scope=='All':
                sites=report_tools.GetProjectInstitutes(client, projCode='S')
        elif testType=='IV_TEST_CONSISTENCY':
            ModuleTestReports(client,sites,compType,testType)
        if sites != []:
            if compType=="HYBRID_ASSEMBLY":
                HybridAssemblyReports(client,sites,compType,testType)
            if compType=="MODULE":
                ModuleTestReports(client,sites,compType,testType)
        

        st.write('---')
        st.header("QC Overview")
        compType=st.selectbox("Choose a component type", ['',]+['HYBRID_ASSEMBLY', 'MODULE', 'PWB'] )
        clusCode=""
        sites=[]
        if compType != "":
            clusCode=st.selectbox("Choose a strip cluster", ['',]+report_tools.STRIP_CLUSTERS)
        if clusCode != "":
            with st.spinner('Getting clutser institutes...'):
                clusSites=report_tools.GetClusterInstitutes(client, clusCode)
            sites=st.multiselect("Choose institutes (component current location)", clusSites, key='choose_inst_qc')
        if sites != []:
            QCOverview(client, sites, compType)


        st.write('---')
        st.header("Inventory")
        sites=[]
        scope=st.radio("Choose the scope of reporting", ['Site-wide', 'Cluster-wide', 'All'], horizontal=True, key='rpt_scope_inv')
        if scope=='Site-wide':
            clusCode=st.selectbox("Choose a strip cluster", ['',]+report_tools.STRIP_CLUSTERS, key='clus_site_wide_inv' )
            if clusCode != "":
                with st.spinner('Getting clutser institutes...'):
                    clusSites=report_tools.GetClusterInstitutes(client, clusCode)
                    sites=st.multiselect("Choose institutes (component current location)", clusSites, key='sites_site_wide_inv')
        if scope=='Cluster-wide':
            clusCode=st.selectbox("Choose a strip cluster", ['',]+report_tools.STRIP_CLUSTERS, key='clus_clus_wide_inv')
            if clusCode != "":
                with st.spinner('Getting clutser institutes...'):
                    sites=report_tools.GetClusterInstitutes(client, clusCode)
        if scope=='All': 
            sites=report_tools.GetProjectInstitutes(client, projCode='S')
        if sites != []:
            Inventory(client, sites)

        st.write('---')
        st.header("Batch QC")
        site=''
        batchType=''
        clusCode=st.selectbox("Choose a strip cluster", ['',]+report_tools.STRIP_CLUSTERS+['NOT_LISTED'], key='choose_clus_batch')
        if clusCode != "":
            if clusCode=='NOT_LISTED':
                site=st.selectbox("Choose institute", ['', 'CERN'], key='choose_inst_not_list_batch')
            else:
                with st.spinner('Getting clutser institutes...'):
                    clusSites=report_tools.GetClusterInstitutes(client, clusCode)
                site=st.selectbox("Choose institute", ['',]+clusSites, key='choose_inst_batch')
        if site != '':
            batchType=st.selectbox("Choose batch type", ['', 'SENSORS_PROD_BATCH', 'BATCH_ABC_WAFER'], key='choose_batch')
        if batchType !='':
            if batchType=='SENSORS_PROD_BATCH':
                batchSpec={
                        'batchType':batchType,
                        'compTypeSpec':[
                            {'compCode': 'SENSOR_HALFMOONS'}
                            ],
                        'compTestSpec':[
                            {'testCode': 'PULL_TEST', 'paraCode': 'MEAN', 'columnName':'Mean-Pull Strength'}, 
                            {'testCode': 'PULL_TEST', 'paraCode': 'STANDARD_DEVIATION', 'columnName':'Sigma-Pull Strength'}
                            ],
                }
                entryLimit=st.number_input('Set a limit on number of batch entries', value=0)
                if entryLimit>0:
                    dfBatchQC=BatchQC(client, site, batchSpec, entryLimit)
                    st.dataframe(dfBatchQC)
        if batchType=='BATCH_ABC_WAFER':
                batchSpec={
                        'batchType':batchType,
                        'compTypeSpec':[
                            {'compCode': 'ABC_WAFER'}
                            ],
                        'compTestSpec':[
                            {'testCode': 'ASIC_SIZE'},
                            {'testCode': 'WAFER_THICKNESS', 'paraCode': 'THICKNESS', 'columnName':'Wafer Thickness [micron]'},
                            {'testCode': 'PULL_TEST', 'paraCode': 'MEAN', 'columnName':'Mean-Pull Strength [g]'}, 
                            {'testCode': 'PULL_TEST', 'paraCode': 'STANDARD_DEVIATION', 'columnName':'Sigma-Pull Strength [g]'}
                            ],
                }
                entryLimit=st.number_input('Set a limit on number of batch entries', value=0)
                if entryLimit>0:
                    BatchQC(client, site, batchSpec, entryLimit)



