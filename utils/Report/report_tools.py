# Initialize with the ITk Database 
import itkdb
import pandas as pd
import numpy as np
import sys 
import os 
import datetime 
import copy 
import streamlit as st 

STRIP_CLUSTERS = ['STRIPS-LC-UKCHINA', 'STRIPS-LC-US', 'STRIPS-LC-DESY', 'STRIPS-LC-FREIBURG', 'STRIPS-LC-VANCOUVER', 'STRIPS-LC-VALENCIA', 'STRIPS-L0-TORONTO', 'STRIPS-L0-BROOKHAVEN', 'STRIPS-L0-BERLIN', 'STRIPS-L0-VANCOUVER', 'STRIPS-L0-UPPSALA', 'STRIPS-L0-VALENCIA', 'STRIPS-L0-PRAGUE']

def sortDataFrameByList(df, df_sorter_key, list):
    df_tmp = df.set_index(df_sorter_key)
    df_tmp = df_tmp.reindex(list)
    df_tmp.reset_index(inplace=True)

    return df_tmp

### use project code to get list of institute codes in cluster
### if no code supplied a list of project codes is returned
@st.cache_resource(show_spinner=False)
def GetProjectInstitutes(_myClient, projCode=None):
    # get list of projects
    # if no project code input then return list of codes
    instList=_myClient.get('listInstitutions', json={})
    if projCode==None:
        print("No project code given. Returning all institutions")
        return [x['code'] for x in instList.data]
    # check institutions on project from list of components per institution
    myInstCodes=[x['code'] for x in instList.data if projCode in list(set(y['code'] for y in x['componentTypes'])) ] 
    print(f"found {len(myInstCodes)} institutes in project")
    return myInstCodes


### use cluster code to get list of institute codes in cluster
### if no code supplied a list of cluster codes is returned
@st.cache_resource(show_spinner=False)
def GetClusterInstitutes(_myClient, clusCode=None):
    # get list of clusters
    clusList=_myClient.get('listClusters', json={})
    # if no cluster code input then return list of codes
    if clusCode==None:
        print("No cluster code given. Listing:")
        print([x['code'] for x in clusList])
        return clusList
    # check cluster code list for input code
    foundItem=next((item for item in clusList if item['code'] == clusCode), None)
    # return None if no matching code found
    if foundItem==None:
        print("No cluster code found. Listing:")
        print([x['code'] for x in clusList])
        return foundItem
    # return institute code list of matching cluster
    myInstCodes=[x['code'] for x in foundItem['instituteList']]
    print(f"found {len(myInstCodes)} institutes in cluster")
    return myInstCodes

### Get list of components (of type in compDict) from institutes in instList
### based on matching currentLocation
def GetComponentInfo(myClient, instList, compDict):
    # list for matching components
    foundComps=[]
    # check input is a list (if not make it one)
    if type(instList)!=type([]):
        print("casting input as list")
        myInstCodes=[instList]
    else:
        myInstCodes=instList
    # loop through codes in list
    for inst in myInstCodes:
        print(f"working on: {inst}")
        # get components with institute as currentLocation
        if compDict['compTypeCode'] == 'all':
            compList=myClient.get('listComponents', json={'project':compDict['projCode'],'state':['ready'], 'currentLocation':inst, 'pageInfo': {'pageSize': 4000}})
        elif 'compSubTypeCode' in compDict.keys(): 
            compList=myClient.get('listComponents', json={'componentType':compDict['compTypeCode'], 'type':compDict['compSubTypeCode'],
                                                    'project':compDict['projCode'],'state':['ready'], 'currentLocation':inst, 'pageInfo': {'pageSize': 4000}})
        else:
            compList=myClient.get('listComponents', json={'componentType':compDict['compTypeCode'], 'project':compDict['projCode'],
                                                        'state':['ready'], 'currentLocation':inst, 'pageInfo': {'pageSize': 4000}}) # 'state'=='ready' to filter out deleted comps
        # add exception for case where code is missing
        compCodes=[]
        for x in compList.data:
            try:
                compCodes.append(x['code'])
            except KeyError:
                print("no code key found. skipping")
                continue
        # try:
        #     compCodes=[x['code'] for x in compList.data]
        # except KeyError:
        #     print("no code key found. skipping")
        #     continue
        
        # add to list of matching components
        foundComps.extend([{'code':c} for c in compCodes])
        print(f"found components: {len(foundComps)}")
    print(f"found components: {len(foundComps)}")
    return foundComps


### Get child component information based on parent codes and compDict
### chunk used to limit size of request to database and avoid timeout errors
def GetChildInfo(myClient, parentCodes, compDict, chunk=100):
    # list to keep components
    childComps=[]
    # get parent components info. in chunks
    foundList=[]
    for x in range(0,int(np.ceil(len(parentCodes)/chunk))):
        print(f"component loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        foundList.extend(myClient.get('getComponentBulk', json={'component': parentCodes[x*chunk:(x+1)*chunk] }) )

    # process component info.
    for e,comp in enumerate(foundList):
        print(f"working on ({e}/{len(foundList)}): {comp['code']}")
        # check children exist
        try:
            if len(comp['children'])<1:
                print("no children found")
                continue
        except KeyError:
            print("no children key found")
            continue
        # check children match input dictionary and keep codes
        try:
            myChildCode=next((item['component']['code'] for item in comp['children'] if item['componentType']['code'] == compDict['compTypeCode'] and item['type']['code'] == compDict['compSubTypeCode']), None)
        except KeyError:
            print('component code KeyError')
            myChildCode=None
        except TypeError:
            print('component code TypeError')
            myChildCode=None
        # check children match input dictionary and keep serialNumbers
        try:
            myChildSN=next((item['component']['serialNumber'] for item in comp['children'] if item['componentType']['code'] == compDict['compTypeCode'] and item['type']['code'] == compDict['compSubTypeCode']), None)
        except KeyError:
            print('component SN KeyError')
            myChildSN=None
        except TypeError:
            print('component SN TypeError')
            myChildSN=None
        # fudge to catch [nan] (or odd returns)
        if type(myChildCode)!=type("str") and type(myChildCode)!=type(None):
            print("fix code type:",type(myChildCode))
            myChildCode=None
        if type(myChildSN)!=type("str") and type(myChildCode)!=type(None):
            print("fix SN type:",type(myChildSN))
            myChildSN=None
        print("found child:",myChildCode)
        # add to list of matching components
        childComps.append({'childCode':myChildCode,'parentCode':comp['code'],'childSN':myChildSN,'parentSN':comp['serialNumber'], 'curLoc':comp['currentLocation']['code']})
    return childComps

### get testRun ID lists of all tests
### (don't have to keep component ID with testRun as comp ID will be returned with testRun info. )
### chunk used to limit size of request to database and avoid timeout errors 
def GetTestRunIDs(myClient, foundComps, chunk=100):
    # dictionary of test types and testRun IDs
    testRunDict={}

    # loop over components
    for x in range(0,int(np.ceil(len(foundComps)/chunk))):
        print(f"component loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        foundList=myClient.get('getComponentBulk', json={'component':foundComps[x*chunk:(x+1)*chunk] })

        # loop over components and extract test information
        for e,comp in enumerate(foundList):
            if len(comp['tests'])<1:
                print(f"no tests for {comp['code']}")
                continue
            # get test ids
            for ct in comp['tests']:
                # print(ct)
                if "code" in ct.keys():
                    # add to dictionary (try: assuming key already exists, exeption: add key)
                    try:
                        testRunDict[ct['code']].extend([tr['id'] for tr in ct['testRuns']])
                    except KeyError:
                        testRunDict[ct['code']]=[tr['id'] for tr in ct['testRuns']]
                else:
                    try:
                        testRunDict["NO_CODE"].extend([tr['id'] for tr in ct['testRuns']])
                    except KeyError:
                        testRunDict["NO_CODE"]=[tr['id'] for tr in ct['testRuns']]

    # print extracted info.
    for k,v in testRunDict.items():
        print(f"{k} : {len(v)}")
    return testRunDict


### get all testRun data based ID codes
### order (parent/child) doesn't matter as testRun info. will specify
### chunk used to limit size of request to database and avoid timeout errors 
def GetTestRunsData(myClient, matchedTestRuns, chunk=100):
    # list of test runs
    testRuns=[]
    # loop over IDs
    for x in range(0,int(np.ceil(len(matchedTestRuns)/chunk))):
        print(f"testRun loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        testRunChunk=myClient.get('getTestRunBulk',json={'testRun':matchedTestRuns[x*chunk:(x+1)*chunk]})
        # add to test run list
        testRuns.extend(testRunChunk)
        print(f"update testRun results: {len(testRuns)}")
    print(f"final testRun results: {len(testRuns)}")
    return testRuns


# code if in dict, as is if not dict, non if no code and dict
def FindKey(x, fk="code"):
    if type(x)==type({}):
        if fk in x.keys():
            return x[fk]
        else:
            return None
    return x


### get/invent test index - used to distinguish repeated testTypes with same institute, serialNumber and stage
def GetIndexStage(inst, sn, stage, combCollection):
    # if this inst NOT recorded then add to list
    if inst not in set([cc['inst'] for cc in combCollection]): ### no inst found
        combCollection.append({'inst':inst,'SN':sn, 'stage': stage})
        # return initial count
        return 0
    else:
        instCollection=[cc for cc in combCollection if cc['inst']==inst] ### get inst collection
        # if this inst-sn combination NOT recorded then add to list
        if sn not in set([ic['SN'] for ic in instCollection]): ### no SN found
            combCollection.append({'inst':inst,'SN':sn, 'stage': stage} )
            # return initial count
            return 0
        else:
            # if this inst-sn combination IS recorded then append toexisint entry in list
            SNCollection=[ic for ic in instCollection if ic['SN']==sn]
            if stage not in set([snc['stage'] for snc in SNCollection]):
                combCollection.append({'inst':inst,'SN':sn, 'stage': stage})
                # return initial count
                return 0
            else:
                stageCollection=[snc for snc in SNCollection if snc['stage']==stage ]
                # return appended count
                return len(stageCollection)
    return -1
    
def GetIndex(inst, sn, combCollection):
    # if this inst NOT recorded then add to list
    if inst not in set([cc['inst'] for cc in combCollection]): ### no inst found
        combCollection.append({'inst':inst,'SN':sn})
        # return initial count
        return 0
    else:
        instCollection=[cc for cc in combCollection if cc['inst']==inst] ### get inst collection
        # if this inst-sn combination NOT recorded then add to list
        if sn not in set([ic['SN'] for ic in instCollection]): ### no SN found
            combCollection.append({'inst':inst,'SN':sn})
            # return initial count
            return 0
        else:
            # if this inst-sn combination IS recorded then append toexisint entry in list
            SNCollection=[ic for ic in instCollection if ic['SN']==sn]
            combCollection.append({'inst':inst,'SN':sn})
            # return appended count
            return len(SNCollection)
    return -1

### Get connection of parent to child
def GetConnection(conList,code):
    # match code to parent in list
    try:
        parMatch = next((item for item in conList if item['parentCode'] == code), None)
    except TypeError:
        print("connection TypeError:",conList)
        return None
    # if no match, try to match code to child in list
    if parMatch==None:
        childMatch = next((item for item in conList if item['childCode'] == code), None)
        if childMatch==None:
            # give up
            return None
        else:
            # return connection
            return str(childMatch['parentSN'])+"-"+str(childMatch['childSN'])
    else:
        # return connection
        return str(parMatch['parentSN'])+"-"+str(parMatch['childSN'])
    return None


### return value form dictionary matching key
def GetDictVal(inDict,vKey,idx=None):
    for k,v in inDict.items():
        if k==vKey:
            # print(f"got {k}: {v}")
            if idx!=None:
                try:
                    return inDict[k][idx]
                except IndexError:
                    print("index error")
                    return None
            else:
                return inDict[k]
        else:
            if type(v)==type({}):
                return GetDictVal(v,vKey)
    return None


### additional formatting to get sub-keys from dictionary objects
def SubKeyFormatting(df_testRuns,testTypes):
    for tt in testTypes:
        print("working on:",tt['paraCode'],"@",tt['testCode'])
        if "subs" in tt.keys():
            print("\tchecking:",tt['subs'])
            df_totSub=pd.DataFrame()
            for pSub in tt['subs']:
                for pKey,pVal in pSub.items():
                    print("\ttry:",pKey)
                    df_sub=df_testRuns.query('paraCode=="'+tt['paraCode']+'"')
        #             display(df_sub)
                    ### if an int --> get index==int from paraValue
                    if type(pVal)==type(1):
                        print("\t\tidx:",pVal)
                        df_sub['paraValue']=df_sub['paraValue'].apply(lambda x: GetDictVal(x,pKey,pVal) )
                        df_sub['paraCode']=tt['paraCode']+"->"+pKey+"["+str(pVal)+"]"
                    ### for the moment just leave everything else alone
                    else:
                    ### if None --> use paraValue as is
                    # if pVal==None or type(pVal)==type([]):
                        print("-- leaving substructure alone supported")
                        df_sub['paraValue']=df_sub['paraValue'].apply(lambda x: GetDictVal(x,pKey) )
                        df_sub['paraCode']=tt['paraCode']+"->"+pKey
                    # ### if an list --> explode the values
                    # elif type(pVal)==type([]):
                    #     print("\t\texplode list:",pVal)
                    #     df_sub['paraValue']=df_sub['paraValue'].apply(lambda x: GetDictVal(x,pKey) )
                    #     df_sub['paraCode']=tt['paraCode']+"->"+pKey
                    #     df_sub=df_sub.explode('paraValue')
                    ### stuck
                    # else:
                    #     print(type(pVal),"not currently supported")
    #                 display(df_sub)
                    if df_totSub.empty:
                        df_totSub=df_sub.copy(deep=True)
                    else:
                        df_totSub=pd.concat([df_totSub,df_sub])
                    print(df_totSub['paraCode'].unique())
            ### drop unformatted rows
            print(f"Update of testRun data for {tt['subs']}")
            print("Previous...")
            print(df_testRuns['paraCode'].unique())
            df_testRuns = df_testRuns[df_testRuns['paraCode'] != tt['paraCode']]
            ### add formatted rows
            df_testRuns = pd.concat([df_testRuns,df_totSub])
            print("Updated...")
            print(df_testRuns['paraCode'].unique())

    return df_testRuns


### get related paraCode to dictionary
def GetRelatedPC(paraCode, paraCodeList, exact=False):
    for pc in paraCodeList:
        # if not looking for exact match then loop through
        if exact==False:
            # if can't find related code then skip
            if paraCode not in pc:
                continue
            else: 
                return pc
        # if exact match sought
        else:
            if paraCode==pc:
                return pc
    return None


# transform python types to valueType in PDB(single / array)
def TYPE(var):
    single_value_types = (int, float, bool, str, bytes, type(None), dict)
    if isinstance(var, single_value_types):
        return 'single'
    elif isinstance(var, list):
        return 'array'

### List of formatting commands
def FormatTestRunData(testData, childInfo=None):
    # convert data to pandas dataFrame

    ### drop missing state
    print(f"pre length: {len(testData)}")
    testData=[td for td in testData if type(td)==type({}) and "state" in td.keys()]
    print(f"post length: {len(testData)}")
    df_testRuns=pd.DataFrame(testData)
    if df_testRuns.empty:
        return df_testRuns
    
    print(df_testRuns.columns)

    ### use subset of data
    df_testRuns=df_testRuns.query('state=="ready"')[['id', 'components','testType','institution','date','properties','results','passed', 'problems']]
    # convert institute info. from dictionary to code
    df_testRuns['institution']=df_testRuns['institution'].apply(lambda x: x['code'])
    df_testRuns['testCode']=df_testRuns['testType'].apply(lambda x: x['code'])
    # convert dateTime format
    df_testRuns['date']= pd.to_datetime(df_testRuns['date'],format='%Y-%m-%dT%H:%M:%S.%f')
    now_time = datetime.datetime.now().astimezone(datetime.timezone.utc)
    pastMonth=datetime.timedelta(days=30)
    past3Months=datetime.timedelta(days=90)
    df_testRuns['testPeriod']=df_testRuns['date'].apply(lambda x: 0 if (now_time-x <= pastMonth) else (2 if now_time-x <= past3Months else 4))
    df_testRuns['passedOrNot']=df_testRuns.apply(lambda row: 'Failed' if not row['passed'] else ("Passed with problems" if row['problems'] else "Passed"), axis=1)

    ### component info. part - unpack dictionary object
    df_testRuns=df_testRuns.explode('components')
    # simple unpacking
    for k in ['serialNumber','alternativeIdentifier','dummy', 'trashed']:
        df_testRuns[k]=df_testRuns['components'].apply(lambda x: x[k] if k in x.keys() else None) #some components don't have SN or altId
    # manual unpacking
    df_testRuns['compCode']=df_testRuns['components'].apply(lambda x: x['code'])
    df_testRuns['compTypeCode']=df_testRuns['components'].apply(lambda x: x['componentType']['code'])
    df_testRuns['compSubTypeCode']=df_testRuns['components'].apply(lambda x: x['type']['code'])
    df_testRuns['projCode']=df_testRuns['components'].apply(lambda x: x['project']['code'])
    df_testRuns['localName']=df_testRuns['components'].apply(lambda x: next((item['value'] for item in x['properties'] if \
                                    (item['code']=="LOCAL_NAME" or item['code']=="LOCALNAME") and item['value'] is not None ), ' ') \
                                    if x['properties'] is not None else ' ' ) # some cases LOCAL_NAME, other cases LOCALNAME :(
    df_testRuns['stage']=df_testRuns['components'].apply(lambda x: x['testedAtStage']['code'])
    # df_testRuns['currentLocation']=df_testRuns['compCode'].apply(lambda x: next((item['curLoc'] for item in childInfo if item['parentCode'] == x), None) )
    if childInfo!=None:
        df_testRuns['connection']=df_testRuns['compCode'].apply(lambda x: GetConnection(childInfo,x))
    # identifier per test
    combCollection=[]
    df_testRuns['testIndex']=df_testRuns.apply(lambda row: GetIndexStage(row['institution'],row['serialNumber'], row['stage'],combCollection), axis=1)

    ###exclude dummy, test and mechanical parts
    df_testRuns=df_testRuns.query('~dummy') 
    df_testRuns=df_testRuns.query('~trashed') 
    df_testRuns=df_testRuns[~df_testRuns['localName'].str.contains('test', case=False)] 
    df_testRuns=df_testRuns[~df_testRuns['localName'].str.contains('dummy', case=False)] 
    df_testRuns=df_testRuns[~df_testRuns['localName'].str.contains('mech', case=False)] 

    ### test info. part
    df_testRuns=df_testRuns.explode('results')
    for k in ["valueType", "dataType"]:
        df_testRuns[k]=df_testRuns['results'].apply(lambda x: x[k] if type(x)!=type(None) else None)
    df_testRuns['paraCode']=df_testRuns['results'].apply(lambda x: x['code'] if type(x)!=type(None) else None)
    df_testRuns['paraValue']=df_testRuns['results'].apply(lambda x: x['value'] if type(x)!=type(None) else None)
    df_testRuns['paraValue']=df_testRuns.apply(lambda row: row['paraValue'] if TYPE(row['paraValue'])==row['valueType'] else None, axis=1) # sometimes parameter with "single" as valueType will have array as paraValue! :(
    # after all unpacking, reset the dataframe index
    df_testRuns=df_testRuns.reset_index(drop=True)

    return df_testRuns

def FormatTestRunDataII(testData, childInfo=None):
    # convert data to pandas dataFrame

    ### drop missing state
    print(f"pre length: {len(testData)}")
    testData=[td for td in testData if type(td)==type({}) and "state" in td.keys()]
    print(f"post length: {len(testData)}")
    df_testRuns=pd.DataFrame(testData)
    if df_testRuns.empty:
        return df_testRuns
    
    print(df_testRuns.columns)

    ### use subset of data
    df_testRuns=df_testRuns.query('state=="ready"')[['id', 'components','testType','institution','date','properties','results','passed', 'problems']]
    # convert institute info. from dictionary to code
    df_testRuns['institution']=df_testRuns['institution'].apply(lambda x: x['code'])
    df_testRuns['testCode']=df_testRuns['testType'].apply(lambda x: x['code'])
    # convert dateTime format
    df_testRuns['date']= pd.to_datetime(df_testRuns['date'],format='%Y-%m-%dT%H:%M:%S.%f')
    now_time = datetime.datetime.now().astimezone(datetime.timezone.utc)
    pastMonth=datetime.timedelta(days=30)
    past3Months=datetime.timedelta(days=90)
    df_testRuns['testPeriod']=df_testRuns['date'].apply(lambda x: 0 if (now_time-x <= pastMonth) else (2 if now_time-x <= past3Months else 4))
    df_testRuns['passedOrNot']=df_testRuns.apply(lambda row: 'Failed' if not row['passed'] else ("Passed with problems" if row['problems'] else "Passed"), axis=1)

    ### component info. part - unpack dictionary object
    df_testRuns=df_testRuns.explode('components')
    # simple unpacking
    for k in ['serialNumber','alternativeIdentifier','dummy', 'trashed']:
        df_testRuns[k]=df_testRuns['components'].apply(lambda x: x[k] if k in x.keys() else None) #some components don't have SN or altId
    # manual unpacking
    df_testRuns['compCode']=df_testRuns['components'].apply(lambda x: x['code'])
    df_testRuns['compTypeCode']=df_testRuns['components'].apply(lambda x: x['componentType']['code'])
    df_testRuns['compSubTypeCode']=df_testRuns['components'].apply(lambda x: x['type']['code'])
    df_testRuns['projCode']=df_testRuns['components'].apply(lambda x: x['project']['code'])
    df_testRuns['localName']=df_testRuns['components'].apply(lambda x: next((item['value'] for item in x['properties'] if \
                                    (item['code']=="LOCAL_NAME" or item['code']=="LOCALNAME") and item['value'] is not None ), ' ') \
                                    if x['properties'] is not None else ' ' ) # some cases LOCAL_NAME, other cases LOCALNAME :(
    df_testRuns['stage']=df_testRuns['components'].apply(lambda x: x['testedAtStage']['code'])
    # df_testRuns['currentLocation']=df_testRuns['compCode'].apply(lambda x: next((item['curLoc'] for item in childInfo if item['parentCode'] == x), None) )
    if childInfo!=None:
        df_testRuns['connection']=df_testRuns['compCode'].apply(lambda x: GetConnection(childInfo,x))
    # identifier per test
    combCollection=[]
    df_testRuns['testIndex']=df_testRuns.apply(lambda row: GetIndexStage(row['institution'],row['serialNumber'], row['stage'],combCollection), axis=1)

    ###exclude dummy, test and mechanical parts
    df_testRuns=df_testRuns.query('~dummy') 
    df_testRuns=df_testRuns.query('~trashed') 
    df_testRuns=df_testRuns[~df_testRuns['localName'].str.contains('test', case=False)] 
    df_testRuns=df_testRuns[~df_testRuns['localName'].str.contains('dummy', case=False)] 
    df_testRuns=df_testRuns[~df_testRuns['localName'].str.contains('mech', case=False)] 

    # after all unpacking, reset the dataframe index
    df_testRuns=df_testRuns.reset_index(drop=True)

    return df_testRuns

### component summary
@st.cache_data(show_spinner=False)
def GetComponentData(_myClient, compInfo, chunk=100):
    # list of test runs
    compList=[]
    # loop over IDs
    for x in range(0,int(np.ceil(len(compInfo)/chunk))):
        print(f"component loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        compChunk=_myClient.get('getComponentBulk',json={'component':[ci['code'] for ci in compInfo[x*chunk:(x+1)*chunk]]})
        compList.extend(compChunk)

    df_comps=pd.DataFrame(compList)

    return df_comps

def FormatComponentData(df_comps):
    df_comps=df_comps.query('state=="ready"')[['code', 'serialNumber', 'componentType','type','institution','currentLocation','properties', 'dummy', 'trashed', 'parents']]
    df_comps['localName']=df_comps['properties'].apply(lambda x: next((item['value'] for item in x if \
                                    (item['code']=="LOCAL_NAME" or item['code']=="LOCALNAME") and item['value'] is not None ), ' ') \
                                    if x is not None else ' ' ) # some cases LOCAL_NAME, other cases LOCALNAME :(
    # get codes if possible
    for k in df_comps.columns:
        df_comps[k]= df_comps[k].apply(lambda x: FindKey(x))

    df_comps=df_comps.query('~dummy') 
    df_comps=df_comps.query('~trashed') 
    df_comps=df_comps[~df_comps['localName'].str.contains('test', case=False)] 
    df_comps=df_comps[~df_comps['localName'].str.contains('dummy', case=False)] 

    df_comps['parentTypes']= df_comps['parents'].apply(lambda x: [p['componentType']['code'] for p in x if p['component'] is not None] if x is not None else [])
    # make sums
    # df_comps['No.Stages']= df_comps['stages'].apply(lambda x: len(x) if x!=None else 0)
    # df_comps['No.Tests']= df_comps['tests'].apply(lambda x: len(x) if x!=None else 0)

    return df_comps


### stage info.
def FormatStageData(myClient, compInfo, chunk=100):
    # list of test runs
    compList=[]
    # loop over IDs
    for x in range(0,int(np.ceil(len(compInfo)/chunk))):
        print(f"component loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        compChunk=myClient.get('getComponentBulk',json={'component':[ci['code'] for ci in compInfo[x*chunk:(x+1)*chunk]]})
        compList.extend(compChunk)

    df_stages=pd.DataFrame(compList)
    # print(df_stages.columns)
    # display(df_comps[['project','componentType','type','institution','currentLocation','currentStage']])   
    df_stages=df_stages[['id','code','project','componentType','serialNumber','alternativeIdentifier','stages','currentLocation','type']]
    # get codes if possible
    for k in df_stages.columns:
        df_stages[k]= df_stages[k].apply(lambda x: FindKey(x))
    # get stage info.
    df_stages=df_stages.explode('stages')
    for k in ['code','dateTime']:
        df_stages['stage_'+k]=df_stages['stages'].apply(lambda x: x[k] if type(x)==type({}) and k in x.keys() else None)
    # necessary hack to deal with strange formatting (datetime.datetime instead of pandas._libs.tslibs.timestamps.Timestamp)
    df_stages['stage_dateTime']= df_stages['stage_dateTime'].apply(lambda x: str(x).split(':')[0] if x!=None else None)
    df_stages['stage_dateTime']= pd.to_datetime(df_stages['stage_dateTime'],format='%Y-%m-%dT%H:%M:%S.%f',errors='coerce')
    df_stages=df_stages.reset_index(drop=True)

    return df_stages


### List of formatting commands
def FormatTestData(myClient, testInfo, chunk=100):
    # list of test runs
    testList=[]
    # loop over IDs
    for x in range(0,int(np.ceil(len(testInfo)/chunk))):
        print(f"test loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        testChunk=myClient.get('getTestRunBulk',json={'testRun':[ti['id'] for ti in testInfo[x*chunk:(x+1)*chunk]]})
        testList.extend(testChunk)

    # convert data to pandas dataFrame
    df_testRuns=pd.DataFrame([x for x in testList if x!=None])

    if df_testRuns.empty:
        return df_testRuns
    # display(df_testRuns)
    print(df_testRuns.columns)   
    # some things don't have state
    if "state" in df_testRuns.columns:
        df_testRuns=df_testRuns.query('state=="ready"')[['components','institution','testType','date','properties','results','passed']]
    else:
        df_testRuns=df_testRuns[['components','institution','testType','date','properties','results','passed']]

    # get codes if possible
    for k in df_testRuns.columns:
        df_testRuns[k]= df_testRuns[k].apply(lambda x: FindKey(x))

    # convert dateTime format
    df_testRuns['date']= pd.to_datetime(df_testRuns['date'],format='%Y-%m-%dT%H:%M:%S.%f')
    # simple unpacking
    for k in ['serialNumber','alternativeIdentifier']:
        try:
            df_testRuns[k]=df_testRuns['components'].apply(lambda x: x[k] if type(x)==type({}) and k in x.keys() else None)
        except KeyError: # except missing
            pass
    # get component info.
    df_testRuns=df_testRuns.explode('components')
    df_testRuns['compCode']=df_testRuns['components'].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else None)
    for k,v in {'compTypeCode':"componentType", 'projCode':"project", 'typeCode':"type", 'stage':"testedAtStage"}.items():
        df_testRuns[k]=df_testRuns['components'].apply(lambda x: x[v]['code'] if type(x)==type({}) and v in x.keys() and type(x[v])==type({}) and "code" in x[v].keys() else None)
    df_testRuns['localName']=df_testRuns['components'].apply(lambda x: next((item['value'] for item in x['properties'] if item['code']=="LOCALNAME"), None) if type(x)==type({}) and "properties" in x.keys() else None)
    # identifier per test
    # combCollection=[]
    # df_testRuns['testIndex']=df_testRuns.apply(lambda row: GetIndex(row['institution'],row['compCode'],combCollection), axis=1)

    ### test info. part
    df_testRuns=df_testRuns.explode('results')
    for k in ["valueType", "dataType"]:
        df_testRuns[k]=df_testRuns['results'].apply(lambda x: x[k] if type(x)!=type(None) else None)
    df_testRuns['paraCode']=df_testRuns['results'].apply(lambda x: x['code'] if type(x)!=type(None) else None)
    df_testRuns['paraValue']=df_testRuns['results'].apply(lambda x: x['value'] if type(x)!=type(None) else None)
    # after all unpacking, reset the dataframe index
    df_testRuns=df_testRuns.reset_index(drop=True)

    return df_testRuns


def FilterPeriod(key):
    testPeriod = st.radio(label="Test Period", options=["Past Month", "Past 3 Months", "All Time"], index=2, horizontal=True, key=key)
    if testPeriod == "Past Month":
        testPeriodFilter=1
    elif testPeriod == "Past 3 Months":
        testPeriodFilter=3
    else:
        testPeriodFilter=5

    return testPeriodFilter

@st.cache_data(show_spinner=False)
def GetComponentTestTypes(_myClient, compTypeCode):
    compType=_myClient.get('getComponentTypeByCode', json={'project': 'S', 'code':compTypeCode})
    df_stages=pd.DataFrame(compType['stages'])
    df_stages=df_stages.explode(['testTypes'])
    df_stages['testCode']=df_stages['testTypes'].apply(lambda x: x['testType']['code'] if type(x) is dict else 'None')
    df_stages['stageAndTestCode']=df_stages.apply(lambda row: '-'.join([row['code'],str(row['testCode'])]), axis=1)
    df_stages=df_stages.rename(columns={'code':'stage'})

    return df_stages

@st.cache_data(show_spinner=False)
def GetComponents(_myClient, compSpec, projCode="S", clusCode=None, instCode=None):
    if clusCode:
        # print(f"found clusCode: {clusCode}")
        instList=GetClusterInstitutes(_myClient, clusCode)
    elif instCode:
        # print(f"found instCode: {instCode}")
        instList=instCode
    else:
        # print(f"no instCode or clusCode found. Using project code {projCode}")
        instList=GetProjectInstitutes(_myClient, projCode)

    compInfo=GetComponentInfo(_myClient, instList, compSpec)
    if "child" in compSpec['filters'].keys():
        childInfo=GetChildInfo(_myClient, [p['code'] for p in compInfo], compSpec['filters']['child'], 100)
        ### tidying
        for ci in childInfo:
            for sn in ['parentSN','childSN']:
                try:
                    # remove non standard Atlas serialNumbers
                    if "20U" not in ci[sn]:
                        ci[sn]=None
                except TypeError:
                    ci[sn]=None
        # fill return list (children only)
        childInfo=[c['childCode'] for c in childInfo]
        # remove Nones - information cannot be processed
        childInfo=[{'code':x} for x in childInfo if x==x and x!=None]
        compInfo=childInfo

    return compInfo

@st.cache_data(show_spinner=False)
def GetTestRuns(_myClient, compInfo):
    compTestRunsInfo=[]
    compTestRuns=GetTestRunIDs(_myClient, [x['code'] for x in compInfo])
    if len(compTestRuns)<1:
        print(f"no test info")
    else:
        compTestRunsInfo.append(compTestRuns)
    return compTestRunsInfo

@st.cache_data(show_spinner=False)
def GetTestRunsDataFiltered(_myClient, compTestRunsInfo, testSpec):
    matchedTestRuns=[]
    if type(testSpec)==type("str") and  testSpec.lower()=="all":
        # print("use *all* for matchedTestRuns")
        for ctri in compTestRunsInfo:
            # print(ctri)
            for k,v in ctri.items():
                # print(k)
                matchedTestRuns.extend(v)
    elif type(testSpec)==type([]):
        # print("use subset for matchedTestRuns")
        for tc in set([x['testCode'] for x in testSpec]):
            for ctri in compTestRunsInfo:
                try:
                    matchedTestRuns.extend(ctri[tc])
                    # print(f"found testType: {tc}")
                except KeyError:
                    # print(f"no matching testType: {tc}")
                    continue
    else:
        # print("don't understand extraction spec:",testSpec)
        matchedTestRuns=None
    ### get test run data
    if matchedTestRuns==None:
        print(f"No matched testRuns, skipping...")
        return None
    else:
        testInfo=GetTestRunsData(_myClient, matchedTestRuns)
        return testInfo



def GetBatchIDs(myClient, instList, batchTypes, entryLimit):
    if type(instList)==str:
        instList=[instList]
    if type(batchTypes)==str:
        batchTypes=[batchTypes]
    batchList=myClient.get('listBatches', json={'filterMap':{'project':'S', 'batchType':list(batchTypes), 'state':['ready'], 'ownerInstitute': list(instList)}})
    if type(batchList)==list:
        if len(batchList)==0:
            return []
    if len(batchList.data)==0:
        return []
    dfBatchList=pd.DataFrame(batchList.data)
    dfBatchList['date']=pd.to_datetime(dfBatchList['cts'],format='%Y-%m-%dT%H:%M:%S.%f')
    dfBatchList.sort_values(by='date', inplace=True, ascending=False)
    foundBatches=list(dfBatchList['id'])[0:entryLimit]
    print(f'found batch: {len(foundBatches)}')
    return foundBatches

